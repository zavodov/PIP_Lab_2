<%@page contentType="text/html" pageEncoding="UTF-8"
        language="java" import="java.util.List, java.util.ArrayList, Lab_2.AreaCheckServlet"
        session="true"
%>

<!DOCTYPE html>

<head>
    <meta charset="utf-8" />
    <title>PIP_Lab_2</title>
    <link rel="shortcut icon" href="img/favicon.ico" />
    <style> <%@include file='css/main.css' %> </style>
    <script type="text/javascript"> <%@include file='js/scripts.js' %> </script>
   <!-- <link rel="stylesheet" href="css/main.css">-->

</head>

<body onload="drawCanwas('canvas',1)">
<center>

    <div class="container header">
        <span class="left">Гр. P3210</span>
        <span class="center">Заводов Андрей / Черных Дмитрий</span>
        <span class="right">Вар. 1013</span>
    </div>

    <div class="container form">
        <form class="form" id="form" name="form" action="checking" method="get" onsubmit="return validate(this);">

            <table class="radio_btn">
                <tr>
                    <td></td>
                    <td><input type="radio" id="-4" name="X" value="-4">-4</td>
                    <td><input type="radio" id="-3" name="X" value="-3">-3</td>
                    <td><input type="radio" id="-2" name="X" value="-2">-2</td>
                </tr>
                <tr>
                    <td> X = </td>
                    <td><input type="radio" id="-1" name="X" value="-1">-1</td>
                    <td><input type="radio" id="0" name="X" value="0" checked>0</td>
                    <td><input type="radio" id="1" name="X" value="1">1</td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="radio" id="2" name="X" value="2">2</td>
                    <td><input type="radio" id="3" name="X" value="3">3</td>
                    <td><input type="radio" id="4" name="X" value="4">4</td>
                </tr>
            </table>

            <label for="Y"> Y = </label>
            <input class="input_Y" id="Y" type="text" name="Y" placeholder="(-3 .. 3)"><br>

            <label for="R_1"> R = </label>
                <input type="radio" onclick="drawCanwas('canvas',1)" id="R_1" name="R" value="1" checked>1
                <input type="radio" onclick="drawCanwas('canvas',2)" id="R_2" name="R" value="2">2
                <input type="radio" onclick="drawCanwas('canvas',3)" id="R_3" name="R" value="3">3
                <input type="radio" onclick="drawCanwas('canvas',4)" id="R_4" name="R" value="4">4
                <input type="radio" onclick="drawCanwas('canvas',5)" id="R_5" name="R" value="5">5<br>

            <input class="submit" type="submit" name="submit" value=" ПРОВЕРИТЬ ">
        </form>

        <canvas id="canvas" onclick="clicCanvas('canvas',document.getElementById('form').R.value)" style="background-color:#ffffff; border-radius: 20px;" width="300" height="300"></canvas>

    </div>


    <div class="container footer">
        <span class="left">
          <a href="https://se.ifmo.ru/">
            <img id="VT_logo" src="img/vt_logo.png" width="50" height="50" alt="logo">
          </a>
        </span>
        <span class="center">ПИП 2017 г.</span>
        <span class="right">Е. Цопа</span>
    </div>

</center>
</body>
