package Lab_2;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AreaCheckServlet", urlPatterns = "/checking")
public class AreaCheckServlet extends HttpServlet {

    private ServletConfig config;
    private HttpSession session;
    private int id;

    @Override
    public void init (ServletConfig config) throws ServletException
    {
        this.config = config;
    }
    @Override
    public void destroy() {}
    @Override
    public ServletConfig getServletConfig()
    {
        return config;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        session = request.getSession();

        if (session.isNew()){
            id = 0;
        }

        try{
             Point p = new Point(Double.parseDouble(request.getParameter("X")),
                    Double.parseDouble(request.getParameter("Y")), Integer.parseInt(request.getParameter("R")));
            p.isInArea = checkArea(p.x, p.y, p.R);
            session.setAttribute(String.valueOf(id), p);
        } catch (Exception e){
            e.printStackTrace();
            request.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }

        String pageTitle="Servlet example";
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML> <html> <head> <meta charset='UTF-8'> <title>Points</title>" +
                "            <link rel='shortcut icon' href='img/favicon.ico'>" +
                "            <link rel='stylesheet' type='text/css' href='css/main.css'>" +
                "            </head> <body> <center>");
        out.println("<div class='container' style='padding:20px 0px;'> <a href='index.jsp'> <button class='submit'> Return to HOME </button> </a> <br>");
        out.println("<br> <table class='points'> <tr><td>X coordinate</td><td>Y coordinate</td><td>Radius</td><td>Entrance</td></tr>");

        for(int i=0;i<=id;i++) {
            Point p = (Point)session.getAttribute(String.valueOf(i));
            out.println("<tr>");
            out.println("<td>");
            out.println(p.x);
            out.println("</td>");
            out.println("<td>");
            out.println(p.y);
            out.println("</td>");
            out.println("<td>");
            out.println(p.R);
            out.println("</td>");
            out.println("<td>");

            if(checkArea(p.x, p.y, p.R)){
                out.println("Yes");
                p.isInArea=true;
            }
            else{
                out.println("No");
                p.isInArea=false;
            }
            config.getServletContext().setAttribute("isInArea", p.isInArea);
            out.println("</td>");
            out.println("</tr>");
        }

        out.println("</table> </body> </html>");
        id++;

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("control");
    }

    public class Point {
       public double x;
       public double y;
       public int R;
       public boolean isInArea;

        Point (double x, double y, int r){
            this.x = x;
            this.y = y;
            this.R = r;
        }
    }

    public static boolean checkArea(double x, double y, int R){
        if(x<=0 && y<=0 && x*x+y*y<=R*R){
            return true;
        }
        if(x<=0 && y>=0 && y<=2*x+R){
            return true;
        }
        if(x>=0 && y<=0 && x<=R && y>=-R){
            return true;
        }
        return false;
    }

}
